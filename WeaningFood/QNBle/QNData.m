//
//  QNData.m
//  qndemo
//
//  Created by YongMoonShin on 2017. 9. 7..
//  Copyright © 2017년 Chironsoft. All rights reserved.
//

#import "QNData.h"


@implementation QNItemData
-(instancetype)init:(QNDataType)type value:(float)value {
    self = [super init];
    self.type = type;
    self.value = value;
    return self;
}
-(NSString *)name {
    return [QNData getName:self.type];
}
-(NSString *)valueStr {
    return [NSString stringWithFormat:@"%.1f", self.value];
}
@end


@interface QNData ()
@property NSMutableArray<QNItemData *> *k;
@end

@implementation QNData

+(NSString *)getKey:(QNDataType)type {
    switch(type) {
        case Weight:
            return @"weight";
        case Bmi:
            return @"bmi";
        case BodyFat:
            return @"bodyfat";
        case SubFat:
            return @"subfat";
        case VisFat:
            return @"visfat";
        case Water:
            return @"water";
        case SkelMuscle:
            return @"muscle";
        case Bone:
            return @"bone";
        case Protein:
            return @"protein";
        case Bmr:
            return @"bmr";
        case BodyShape:
            return @"body_shape";
        case FatFreeWeight:
            return @"fat_free_weight";
//        case Whr:
//            return @"S라인";
        case FattyLiverRisk:
            return @"fatty_liver_risk";
        case BodyAge:
            return @"bodyage";
        case SiNew:
            return @"sinew";
        case Score:
            return @"score";
        case Resistance:
            return @"resistance";
        case ResistanceSecond:
            return @"resistance_second2";
        default:
            return nil;
    }
}
+(NSString *)getName:(QNDataType)type {
    switch(type) {
        case Weight:
            return @"체중";
        case Bmi:
            return @"BMI";
        case BodyFat:
            return @"체지방률";
        case SubFat:
            return @"피하지방";
        case VisFat:
            return @"내장지방";
        case Water:
            return @"체수분";
        case SkelMuscle:
            return @"골격근비율";
        case Bone:
            return @"골량";
        case Protein:
            return @"단백질";
        case Bmr:
            return @"기초대사량";
        case BodyShape:
            return @"체형";
        case FatFreeWeight:
            return @"제지방량";
        case Whr:
            return @"S라인";
        case FattyLiverRisk:
            return @"지방간위험";
        case BodyAge:
            return @"신체연령";
        case SiNew:
            return @"근육량";
        case Score:
            return @"점수";
        case Resistance:
            return @"임피던스";
        case ResistanceSecond:
            return @"임피던스2";
        default:
            return nil;
    }
}

-(instancetype)init {
    self = [super init];
    self.k = [[NSMutableArray alloc] init];
    return self;
}
-(instancetype)initWithData:(NSDictionary *)data {
    self = [self init];
    
    NSString *createTime = (NSString *) data[@"measure_time"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    self.createTime = [dateFormatter dateFromString:createTime];
    self.userId   = (NSString *)data[@"user_id"];
    self.weightUnit=(NSString *)data[@"weight_unit"];
    self.weight  = ((NSNumber *)data[[QNData getKey:Weight       ]]).floatValue;
    self.bmi     = ((NSNumber *)data[[QNData getKey:Bmi          ]]).floatValue;
    self.bodyfat = ((NSNumber *)data[[QNData getKey:BodyFat      ]]).floatValue;
    self.water   = ((NSNumber *)data[[QNData getKey:Water        ]]).floatValue;
    self.bmr     = ((NSNumber *)data[[QNData getKey:Bmr          ]]).floatValue;
    self.subfat  = ((NSNumber *)data[[QNData getKey:SubFat       ]]).floatValue;
    self.visfat  = ((NSNumber *)data[[QNData getKey:VisFat       ]]).floatValue;
    self.sMuscle = ((NSNumber *)data[[QNData getKey:SkelMuscle   ]]).floatValue;
    self.muscle  = ((NSNumber *)data[[QNData getKey:SiNew        ]]).floatValue;
    self.bone    = ((NSNumber *)data[[QNData getKey:Bone         ]]).floatValue;
    self.protein = ((NSNumber *)data[[QNData getKey:Protein      ]]).floatValue;
    self.score   = ((NSNumber *)data[[QNData getKey:Score        ]]).floatValue;
    self.bodyAge = ((NSNumber *)data[[QNData getKey:BodyAge      ]]).floatValue;
    self.fWeight = ((NSNumber *)data[[QNData getKey:FatFreeWeight]]).floatValue;
    self.bodyShape = ((NSNumber *)data[[QNData getKey:BodyShape  ]]).floatValue;
    [self.k addObject:[[QNItemData alloc] init:Weight        value:self.weight ]];
    [self.k addObject:[[QNItemData alloc] init:Bmi           value:self.bmi    ]];
    [self.k addObject:[[QNItemData alloc] init:BodyFat       value:self.bodyfat]];
    [self.k addObject:[[QNItemData alloc] init:Water         value:self.water  ]];
    [self.k addObject:[[QNItemData alloc] init:Bmr           value:self.bmr    ]];
    [self.k addObject:[[QNItemData alloc] init:SubFat        value:self.subfat ]];
    [self.k addObject:[[QNItemData alloc] init:VisFat        value:self.visfat ]];
    [self.k addObject:[[QNItemData alloc] init:SkelMuscle    value:self.sMuscle]];
    [self.k addObject:[[QNItemData alloc] init:SiNew         value:self.muscle ]];
    [self.k addObject:[[QNItemData alloc] init:Bone          value:self.bone   ]];
    [self.k addObject:[[QNItemData alloc] init:Protein       value:self.protein]];
    [self.k addObject:[[QNItemData alloc] init:Score         value:self.score  ]];
    [self.k addObject:[[QNItemData alloc] init:BodyAge       value:self.bodyAge]];
    [self.k addObject:[[QNItemData alloc] init:FatFreeWeight value:self.fWeight]];
    [self.k addObject:[[QNItemData alloc] init:BodyShape     value:self.bodyShape]];
    
    return self;
}
-(NSArray<QNItemData *> *)getAll {
    return self.k;
}

@end
