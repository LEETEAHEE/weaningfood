//
//  QNBleManager.h
//  qndemo
//
//  Created by YongMoonShin on 2017. 9. 7..
//  Copyright © 2017년 Chironsoft. All rights reserved.
//

#ifndef _QN_BLE_MANAGER_H_
#define _QN_BLE_MANAGER_H_

#import <Foundation/Foundation.h>
#import "QingNiuSDK.h"
#import "QNData.h"

@protocol QNBleScanDelegate <NSObject>
-(void)onScan:(QingNiuDevice *)device;
@end

@protocol QNBleDelegate <NSObject>
-(void)onConnectStart:(QingNiuDevice *)device;
-(void)onConnected:(QingNiuDevice *)device;
-(void)onDisconnected:(QingNiuDevice *)device;
-(void)onUnsteady:(QingNiuDevice *)device weight:(float)weight;
-(void)onReceived:(QingNiuDevice *)device data:(NSMutableDictionary *)data;
-(void)onReceived:(QingNiuDevice *)device storeData:(NSArray<NSMutableDictionary *> *)datas;
-(void)onReceivedModelUpdate:(QingNiuDevice *)device;
@end

@interface QNBleManager : NSObject <QNBleScanDelegate>
@property QingNiuUser *user;
@property NSMutableArray<NSString *> *devices;
@property NSMutableArray<QingNiuDevice *> *connected;
-(void)setTestMode:(bool)testMode;
-(bool)serviceIsOn;
-(void)setServiceOn:(bool)serviceOn;
-(void)setUnit:(QingNiuWeightUnit)unit;
-(void)unit:(void (^)(QingNiuWeightUnit qingNiuWeightUnit))block;
+(instancetype)getInstance:(id<QNBleDelegate>)delegate;
-(void)scanNewDevice:(id<QNBleScanDelegate>)delegate;
-(void)stopScan;
-(bool)isScanningNewDevice;
-(void)firstConnect:(QingNiuDevice *)bleDevice;
-(void)addDevice:(NSString *)device;
-(void)addDevices:(NSArray<NSString *> *)devices;
-(bool)removeDevice:(NSString *)device;
-(void)removeAllDevices;
@end

#endif /* _QN_BLE_MANAGER_H_ */
