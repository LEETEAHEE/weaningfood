//
//  QNData.h
//  qndemo
//
//  Created by YongMoonShin on 2017. 9. 7..
//  Copyright © 2017년 Chironsoft. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    Weight  = 2,
    Bmi     = 3,
    BodyFat = 4,
    SubFat  = 5,
    VisFat  = 6,
    Water   = 7,
    SkelMuscle = 9,
    Bone    = 10,
    Protein = 11,
    Bmr     = 12,
    Whr     = 15,
    FattyLiverRisk = 16,
    BodyShape = 13,
    FatFreeWeight = 14,
    BodyAge = 17,
    SiNew   = 18,
    Score   = 19,
    Resistance = 20,
    ResistanceSecond = 23,
} QNDataType;

@interface QNItemData : NSObject
-(instancetype)init:(QNDataType)type value:(float)value;
@property QNDataType type;
-(NSString *)name;
@property float value;
-(NSString *)valueStr;
@end

@interface QNData : NSObject
+(NSString *)getKey:(QNDataType)type;
+(NSString *)getName:(QNDataType)type;
@property float bone;
@property float water;
@property float visfat;
@property float bmi;
@property float weight;
@property NSString *weightUnit;
@property float subfat;
@property float protein;
@property float bodyfat;
@property NSString *userId;
@property float sMuscle;
@property float muscle;
@property int bmr;
@property float score;
@property float bodyAge;
@property float bodyFat;
@property float fWeight;
@property float bodyShape;
@property NSDate *createTime;
-(instancetype)initWithData:(NSDictionary *)data;
-(NSMutableArray<QNItemData *> *)getAll;
@end
