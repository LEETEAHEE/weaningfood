//
//  QNBleManager.m
//  qndemo
//
//  Created by YongMoonShin on 2017. 9. 7..
//  Copyright © 2017년 Chironsoft. All rights reserved.
//

#import "QNBleManager.h"

@interface QNBleManager()
@property bool serviceState;
@property id<QNBleDelegate> delegate;
@property bool scanningNewDevice;
@property bool testMode;
@end

@implementation QNBleManager

-(bool)serviceIsOn {
    return _serviceState;
}
-(void)setServiceOn:(bool)serviceOn {
    if (!(_serviceState = serviceOn)) {
        [self disconnectAllDevices];
        [_connected removeAllObjects];
    }
    [self refreshScan];
}
-(void)setUnit:(QingNiuWeightUnit)unit {
    NSLog(@"QingNiuSDK setWeightUnit: %li", (long)unit);
    [QingNiuSDK setWeightUnit:unit];
}
-(void)unit:(void (^)(QingNiuWeightUnit qingNiuWeightUnit))block {
    NSLog(@"QingNiuSDK getCurrentWeightUnit: %@", block);
    [QingNiuSDK getCurrentWeightUnit:block];
}

+(instancetype)getInstance:(id<QNBleDelegate>)delegate {
    static QNBleManager *instance = nil;
    if (instance == nil) {
        instance = [[self alloc] init];
        instance.devices = [[NSMutableArray alloc] init];
        instance.connected = [[NSMutableArray alloc] init];
        instance.testMode = false;
        //打印开关
        //        [QingNiuSDK setLogFlag:YES];
        [QingNiuSDK setWeightUnit:QingNiuRegisterWeightUnitKg];
        //注册轻牛APP
        [QingNiuSDK registerApp:@"seven_2017121421" registerAppBlock:^(QingNiuRegisterAppState qingNiuRegisterAppState) {
            NSLog(@"QingNiuSDK 초기화 테스트 결과: %ld",(long)qingNiuRegisterAppState);
            [instance refreshScan];
        }];
        //        [QingNiuSDK serReceiverOrIgnoreStorageData] // iOS엔 없음??
    }
    instance.delegate = delegate;
    return instance;
}

-(void)scanNewDevice:(id<QNBleScanDelegate>)delegate {
    [QingNiuSDK stopBleScan];
    _scanningNewDevice = true;
    [self startLeScan:delegate];
}
-(void)stopScan {
    [self refreshScan];
}
-(bool)isScanningNewDevice {
    return _scanningNewDevice;
}
-(void)firstConnect:(QingNiuDevice *)bleDevice {
    [self connectDevice:bleDevice];
}

-(void)addDevice:(NSString *)device {
    [_devices addObject:device];
    [self refreshScan];
}
-(void)addDevices:(NSArray<NSString *> *)devices {
    [_devices addObjectsFromArray:devices];
    [self refreshScan];
}
-(bool)removeDevice:(NSString *)device {
    for (int i=0; i<_devices.count; i++) {
        NSString *d = [_devices objectAtIndex:i];
        if ([d isEqualToString:device]) {
            [_devices removeObjectAtIndex:i];
            [self disconnectDevice:device];
            return true;
        }
    }
    return false;
}
-(void)removeAllDevices {
    [_devices removeAllObjects];
    [self disconnectAllDevices];
}
-(void)refreshScan {
    _scanningNewDevice = false;
    [QingNiuSDK stopBleScan];
    if (_serviceState) {
        [self startScan];
    } else {
        [self disconnectAllDevices];
    }
}
-(void)startScan {
    if ((_devices.count > _connected.count) || _testMode) {
        [self startLeScan:self];
    }
}
-(void)onScan:(QingNiuDevice *)bleDevice {
    NSString *mac = bleDevice.macAddress;
    NSLog(@"onScan: %@", mac);
    
    if (_testMode) {
        // 테스트용으로 저장된 기기 상관없이 무조건 연결
        [self connectDevice:bleDevice];
    } else {
        if ([QNBleManager contains:mac on:_devices]) {
            bool connected = false;
            for (QingNiuDevice *qnBleDevice in _connected) {
                if ([qnBleDevice.macAddress isEqualToString:mac]) {
                    connected = true;
                    break;
                }
            }
            if (!connected) {
                [self connectDevice:bleDevice];
            }
        }
    }
}
-(void)onConnectStart:(QingNiuDevice *)device {
    [_delegate onConnectStart:device];
}
-(void)onConnected:(QingNiuDevice *)device {
    if (!_scanningNewDevice) {
        [_connected addObject:device];
        //        [self startScan];
    }
    [_delegate onConnected:device];
}
-(void)onDisconnected:(QingNiuDevice *)device {
    if (!_scanningNewDevice) {
        bool isExisted = false;
        NSString *mac = device.macAddress;
        for (int i=0; i<_connected.count; i++) {
            QingNiuDevice *qnBleDevice = [_connected objectAtIndex:i];
            if ([qnBleDevice.macAddress isEqualToString:mac]) {
                [_connected removeObjectAtIndex:i];
                isExisted = true;
                break;
            }
        }
        if (isExisted) [self refreshScan];
    }
    [_delegate onDisconnected:device];
}

-(void)startLeScan:(id<QNBleScanDelegate>)delegate {
    [QingNiuSDK startBleScan:nil
            scanSuccessBlock:
     ^(QingNiuDevice *qingNiuDevice) {
         [delegate onScan:qingNiuDevice];
     }
               scanFailBlock:
     ^(QingNiuScanDeviceFail qingNiuScanDeviceFail) {
         
     }];
}
-(void)connectDevice:(QingNiuDevice *)bleDevice {
    [QingNiuSDK connectDevice:bleDevice
                         user:_user
              onLowPowerBlock:^() {}
          connectSuccessBlock:
     ^(NSMutableDictionary *deviceData, QingNiuDeviceConnectState qingNiuDeviceConnectState) {
         switch (qingNiuDeviceConnectState) {
             case QingNiuDeviceConnectStateConnectedSuccess:
                 [self onConnected:bleDevice];
                 break;
             case QingNiuDeviceConnectStateDisConnected:
                 [self onDisconnected:bleDevice];
                 break;
             case QingNiuDeviceConnectStateIsWeighting:
                 [self onUnsteady:bleDevice data:deviceData];
                 break;
             case QingNiuDeviceConnectStateWeightOver:
                 [self onReceived:bleDevice data:deviceData];
                 break;
             case QingNiuDeviceConnectStateIsGettingSavedData:
                 [self onReceived:bleDevice storeData:deviceData];
                 break;
             case QingNiuDeviceConnectStateGetSavedDataOver:
                 NSLog(@"存储数据接收完毕：%@",deviceData);
                 break;
             default:
                 break;
         }
     }
             connectFailBlock:
     ^(QingNiuDeviceConnectState qingNiuDeviceConnectState) {
         [self.delegate onDisconnected:bleDevice];
     }];
}
-(void)disconnectAllDevices {
    for (QingNiuDevice *qnDevice in _connected) {
        [self disconnect:qnDevice];
    }
}
-(void)disconnectDevice:(NSString *)device {
    for (QingNiuDevice *qnDevice in _connected) {
        if ([[qnDevice macAddress] isEqualToString:device]) {
            [self disconnect:qnDevice];
            break;
        }
    }
}
-(void)disconnect:(QingNiuDevice *)device {
    [QingNiuSDK cancelConnect:device
          disconnectFailBlock:
     ^(QingNiuDeviceDisconnectState qingNiuDeviceDisconnectState) {
         //         [self onDisconnected:device];
     }
       disconnectSuccessBlock:
     ^(QingNiuDeviceDisconnectState qingNiuDeviceDisconnectState) {
         [self onDisconnected:device];
     }];
}
-(void)onUnsteady:(QingNiuDevice *)bleDevice data:(NSMutableDictionary *)deviceData {
    @try {
        NSNumber *weight = deviceData[@"weight"];
        [_delegate onUnsteady:bleDevice weight:weight.floatValue];
    } @catch (NSException *exception) {
        NSLog(@"实时体重 fail");
    }
}
-(void)onReceived:(QingNiuDevice *)bleDevice data:(NSMutableDictionary *)deviceData {
    //    QNData *data = [[QNData alloc] init];
    
    [_delegate onReceived:bleDevice data:deviceData];
}
-(void)onReceived:(QingNiuDevice *)bleDevice storeData:(NSMutableDictionary *)deviceData {
    //    [_delegate onReceived:bleDevice storeData:<#(NSArray<NSMutableDictionary *> *)#>]
    NSLog(@"storeData 안 들어오는 거 맞지??");
    [_delegate onReceived:bleDevice data:deviceData];
}

+(bool)contains:(NSString *)string on:(NSArray<NSString *> *)strs {
    for (NSString *str in strs) {
        if ([str isEqualToString:string]) {
            return true;
        }
    }
    return false;
}

@end


