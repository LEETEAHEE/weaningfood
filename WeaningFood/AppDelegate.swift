//
//  AppDelegate.swift
//  WeaningFood
//
//  Created by ghsong on 2018. 10. 8..
//  Copyright © 2018년 chironsoft. All rights reserved.
//

import UIKit
import UserNotifications
import NaverThirdPartyLogin
import KakaoOpenSDK
import FBSDKCoreKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    /* 알람 클릭 시 해당 화면으로 이동하도록 url 설정 */
    static let alarmTypes = ["수면", "모유", "분유", "이유식"];
    static let redirectUrlForAlarms = [
            "intake/sleep/"
        ,   "intake/mothermilk/"
        ,   "intake/milkpowderlist/"
        ,   "intake/weaningfoodlist/"
    ];

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // 알람 설정 (options에서 alert는 경고창출력 , sound는 소리나도록, .badge는 앱아이콘에 숫자표시)
        if #available(iOS 10, *) {
            let nc = UNUserNotificationCenter.current();
            nc.requestAuthorization(options: [.alert, .sound, .badge]) { (didAllow, Error) in };
            nc.delegate = self;
            application.registerForRemoteNotifications();
            print("registerForRemoteNotifications");
        }
        
        DispatchQueue.main.async {
            let naver = NaverThirdPartyLoginConnection.getSharedInstance()!;
            naver.isInAppOauthEnable = true;
            naver.isNaverAppOauthEnable = true;
            naver.isOnlyPortraitSupportedInIphone();
            naver.serviceUrlScheme = kServiceAppUrlScheme;
            naver.consumerKey    = NSLocalizedString("naver_client_id"    , comment: "");
            naver.consumerSecret = NSLocalizedString("naver_client_secret", comment: "");
            naver.appName        = NSLocalizedString("naver_client_name"  , comment: "");
            
            FBSDKApplicationDelegate.sharedInstance()?.application(application, didFinishLaunchingWithOptions: launchOptions);
        };
        
        Thread.sleep(forTimeInterval: 0.2);
        return true
    }
    
    // APNS 관련?
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.reduce("", {$0 + String(format: "%02X", $1)});
        print("APNs token: " + token);
        UserDefaults.standard.set(token, forKey: "token");
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("APNs fail: \(error)");
    }

    func applicationWillResignActive(_ application: UIApplication) {
        
    }

    var isFirstForeground = true;
    func applicationDidEnterBackground(_ application: UIApplication) {
        isFirstForeground = false;
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // 다른 앱(바탕화면) 갔다가 돌아올 때
        if let view = ViewController.this {
            if let urls = view.wvMain.url?.absoluteString.split(separator: "?")[0].split(separator: "/") {
                if (urls.count >= 4) {
                    if (urls[1].elementsEqual(view.domain)) {
                        if (urls[2].elementsEqual("weaningfood")) {
                            if (urls[3].elementsEqual("settings")) {
                                // 세팅 화면으로 돌아올 경우 알림 설정 다시 가져오기
                                let on = UIApplication.shared.isRegisteredForRemoteNotifications;
                                view.script("onGetPushStatus(\(String(on)))");
                            }
                        }
                    }
                }
            }
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        print("application open url: " + url.absoluteString);
        
        if let view = ViewController.this {
            if let result = view.handleResult(app, open: url, options: options) {
                return result;
            }
        }
        
        return true;
    }
    
    func getString(_ name: String) -> String {
        return NSLocalizedString(name, comment: "");
    }
    
    func getAlarmList() -> [[String:Any]] {
        if let json = UserDefaults.standard.string(forKey: "alarms") {
            if let data = json.data(using: String.Encoding.utf8) {
                do {
                    return try JSONSerialization.jsonObject(with: data, options: []) as! [[String:Any]];
                } catch {
                    return [];
                }
            }
        }
        return [];
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        // foreground 상태에서도 알람 뜨도록
        completionHandler([.alert, .badge, .sound]);
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let alarms = getAlarmList();
        for alarm in alarms {
            if let index = alarm["index"] as? String {
                if (index.elementsEqual(response.notification.request.identifier)) {
                    let type = alarm["type"] as! Int;
                    let redirectUrl = getString("SERVER_URL") + AppDelegate.redirectUrlForAlarms[type];
                    
                    if (UIApplication.shared.applicationState != .active && isFirstForeground) {
                        // 앱 꺼져있을 땐 실행 시 이동
                        UserDefaults.standard.set(redirectUrl, forKey: "redirectByAlarm");
                        UserDefaults.standard.synchronize();
                        
                    } else {
                        // 앱 켜져있을 땐 바로 이동
                        if let view = ViewController.this {
                            view.wvMain.load(URLRequest(url: URL(string: redirectUrl)!));
                        }
                    }
                    break;
                }
            }
        }
    }
}

