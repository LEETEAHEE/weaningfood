//
//  DeviceItemTableViewCell.swift
//  WeaningFood
//
//  Created by ghsong on 20/12/2018.
//  Copyright © 2018 chironsoft. All rights reserved.
//

import UIKit

class DeviceItem: UITableViewCell {
    
    @IBOutlet weak var ivScale: UIImageView!
    @IBOutlet weak var tvName: UILabel!
    @IBOutlet weak var tvMac: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func set(_ device: QingNiuDevice) -> DeviceItem {
        if (device.model.elementsEqual("CS20C3")) {
            ivScale.image = UIImage(named: "CS20C3");
        } else if (device.model.starts(with: "CS20C")) {
            ivScale.image = UIImage(named: "CS20C1");
        } else {
            ivScale.image = UIImage(named: "CS20M");
        }
        tvName.text = device.model;
        tvMac .text = device.macAddress;
        return self;
    }
}
