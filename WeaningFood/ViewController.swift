//
//  ViewController.swift
//  WeaningFood
//
//  Created by ghsong on 2018. 10. 8..
//  Copyright © 2018년 chironsoft. All rights reserved.
//

import UIKit
import WebKit
import UserNotifications // 알람
import NaverThirdPartyLogin // 네이버 로그인
import KakaoOpenSDK // 카카오 sdk
import FBSDKCoreKit // 페이스북 sdk
import FBSDKLoginKit // 페이스북 sdk
import JavaScriptCore

class ViewController: UIViewController {
    
    var isDebug = false;
    
    func Print(_ item: Any) {
        if (isDebug) {
            print(item);
        }
    }
    
    var sn: SharedBluetooth?;
    
    static var this: ViewController?;
    @IBOutlet weak var viewHead: UIView!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewLayout: UIView!
    @IBOutlet weak var viewProgress: UIView!
    @IBOutlet weak var viewProgressTop: UIView!
    @IBOutlet weak var viewProgressHeader: UIView!
    @IBOutlet weak var indicatorMain: UIActivityIndicatorView!
    var wvMain: WKWebView!
    var domain = "";
    var naver: NaverThirdPartyLoginConnection!
    
    //window.open(), window.close() 처리
    var createWebView: WKWebView?
 
    
    let colorLogin = UIColor(red: 0xff/255.0, green: 0xf8/255.0, blue: 0xf3/255.0, alpha: 1);
    var colorPrimary = UIColor.white; // viewDidLoad에서 재선언
    
    // 웹페이지 다 열리기 전에 스크립트 호출 시 쌓아두기
    var isLoaded = false;
    var afterLoads: [String] = [];
    
    var navigating: URL?;
    var statusCode = 0;
    
    // 안드로이드의 onActivityResult랑 비슷하게 쓰려고 만듦
    var requestCode = 0;
    let REQUEST_ADD_SERVICE = 1;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        Print("viewDidLoad");
        
        #if DEBUG
        isDebug = true;
        #endif
        
        sn = SharedBluetooth.getInstance();
        sn?.viewController = self;
        
        ViewController.this = self;
        
        if let color = viewProgressTop.backgroundColor {
            colorPrimary = color;
        }
        
        var top = CGFloat(20);
        if #available(iOS 11.0, *) {
            top = 0;
            if let delegate = UIApplication.shared.delegate {
                if let keyWindow = delegate.window! {
                    let height = 66 + keyWindow.safeAreaInsets.top;
                    let frame = viewProgressHeader.frame;
                    viewProgressHeader.frame = CGRect(x: frame.origin.x, y: frame.origin.y, width: frame.size.width, height: height);
                }
            }
        }
        
        indicatorMain.transform = CGAffineTransform(scaleX: 3, y: 3);
        
        // storyboard에서 생성 불가, 코드상에서 해야 함
        wvMain = WKWebView();
        wvMain.translatesAutoresizingMaskIntoConstraints = false;
        viewMain.addSubview(wvMain);
        wvMain.leadingAnchor .constraint(equalTo: viewMain.leadingAnchor ).isActive = true;
        wvMain.topAnchor     .constraint(equalTo: viewMain.topAnchor, constant: top).isActive = true;
        wvMain.trailingAnchor.constraint(equalTo: viewMain.trailingAnchor).isActive = true;
        wvMain.bottomAnchor  .constraint(equalTo: viewMain.bottomAnchor  ).isActive = true;
        
        wvMain.navigationDelegate = self;
        wvMain.uiDelegate = self;
        initScriptMessageHandler();
        
        naver = NaverThirdPartyLoginConnection.getSharedInstance();
        naver.delegate = self;
        let url = getString("SERVER_URL");
        var urls = url.split(separator: "/");
        domain = String(urls[1]);
        wvMain.load(URLRequest(url: URL(string: url)!));
        
        if let token = FBSDKAccessToken.current() {
            Print(token);
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Print("viewWillAppear");
        if (requestCode > 0) {
            onActivityResult(requestCode);
            requestCode = 0;
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        Print("viewDidAppear");
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        Print("viewWillDisappear");
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        Print("viewDidDisappear");
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // 상단바 글씨 검정/흰색
    var thisStatusBarStyle = UIStatusBarStyle.default;
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return thisStatusBarStyle;
    }
    func setStatusBarStyle(_ style: UIStatusBarStyle) {
        thisStatusBarStyle = style;
        setNeedsStatusBarAppearanceUpdate();
    }
    
    // 안드로이드처럼 쓰려고 만듦
    func getString(_ name: String) -> String {
        return NSLocalizedString(name, comment: "");
    }
    
    func script(_ script: String) {
        if isLoaded {
            // 페이지가 열린 상태면 스크립트 바로 호출
            evaluateJavaScript(script);
        } else {
            // 페이지가 안 열린 상태면 스크립트 대기열 추가
            // 왠지 아이폰에선 이런 경우가 발생함
            afterLoads.append(script);
        }
    }
    
    func evaluateJavaScript(_ javascript: String) {
        Print("evaluateJavaScript: \(javascript)");
        wvMain.evaluateJavaScript(javascript) { (result, error) in
            if let result = result {
                ViewController.this?.Print(result);
            }
            if let error = error {
                ViewController.this?.Print(error);
            }
        }
    }
    
    func doAfterLoadsAndClear() {
        // 스크립트 대기열 있으면 실행
        for script in afterLoads {
            Print("afterLoad: \(script)");
            evaluateJavaScript(script);
        }
        afterLoads.removeAll();
    }
    
    func onActivityResult(_ requestCode: Int) {
        switch requestCode {
        case REQUEST_ADD_SERVICE:
            sn?.stopScan();
            script("initScale()");
            break;
        default:
            break;
        }
    }
    
}

/**
 * 브라우저 기본 기능 관리
 */
extension ViewController: WKNavigationDelegate {
    
    // 웹페이지 이동 시작 시
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
        Print("didStartProvisionalNavigation: \(String(describing: navigation))")
        var isInLayout = false;
        if webView.url?.absoluteString != "" {
            if let urls = webView.url?.absoluteString.split(separator: "?")[0].split(separator: "/") {
                // "http://localhost/a"일 때
                // JAVA : ["http:", "", "localhost", "a"]
                // Swift: ["http:", "localhost", "a"]
                // 배열 번호 하나씩 어긋남
                if (urls[1].elementsEqual(domain)) {
                    if (urls.count >= 3) {
                        isInLayout = urls[2].elementsEqual("weaningfood")
                            || urls[2].elementsEqual("sample")
                            || urls[2].elementsEqual("join")
                            || urls[2].elementsEqual("recommend");
                        if (!isInLayout) {
                            if (urls.count >= 4) {
                                if (urls[2].elementsEqual("member")) {
                                    if (urls[3].elementsEqual("terms")) {
                                        isInLayout = true;
                                    } else if (urls[3].elementsEqual("logout")) {
                                        Print("logout")
                                        jsInterface("setAutoLogin", body: "{\"autologin\":false}");
                                    }
                                }
                            }
                        }
                    }
                }
                
                if (urls.count >= 4 && urls[3].elementsEqual("main")) {
                    setStatusBarStyle(.default);
                } else {
                    setStatusBarStyle(.lightContent);
                }
                
                // 페이지에 따른 상단바 색상
                if (urls.count == 2) {
                    // 안드로이드는 "~/"로 접속했을 때 url이 "~/member/login"으로 리다이렉트한 주소가 나오는데, 아이폰은 "~/" 그대로임
                    setStatusBarColor(colorLogin, withBlackText: true);
                } else if (urls.count >= 4) {
                    if (urls[3].elementsEqual("login")
                        || urls[3].elementsEqual("logout")) {
                        setStatusBarColor(colorLogin, withBlackText: true);
                    } else if (urls[3].starts(with: "main")) {
                        setStatusBarColor(UIColor.white, withBlackText: true);
                    } else {
                        setStatusBarColor(colorPrimary, withBlackText: false);
                    }
                } else {
                    setStatusBarColor(colorPrimary, withBlackText: false);
                }
            }
        }
        
        viewProgressHeader.isHidden = !isInLayout;
        viewLayout.backgroundColor = (isInLayout ? UIColor.white : UIColor.clear);
        viewLayout.isHidden = false;
        viewProgress.isHidden = false;
        
        // 체중계 측정 도중 나갈 때
        stopScanDevice();
        
        Print(webView.url!.absoluteURL);
        navigating = webView.url;
        isLoaded = false;
    }
    
    func setStatusBarColor(_ color: UIColor, withBlackText: Bool) {
        viewHead.backgroundColor = color;
        
        // 인자 없이 색깔 밝기 계산해서 처리할 수도...?
        if (withBlackText) {
            setStatusBarStyle(.default);
        } else {
            setStatusBarStyle(.lightContent);
        }
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        
        var policy = WKNavigationResponsePolicy.allow;
        
        if navigationResponse.response.isKind(of: HTTPURLResponse.self) {
            let response = navigationResponse.response as! HTTPURLResponse;
            
            statusCode = response.statusCode;
            Print("response: \(statusCode)");
            
            switch (statusCode / 100) {
            case 1, 4, 5:
                policy = WKNavigationResponsePolicy.cancel;
                break;
            default:
                break;
            }
        }
        decisionHandler(policy);
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        Print("didFinish: \(String(describing: navigation))");
        isLoaded = true;
        // 안드로이드처럼 history 클리어할 필요 없음
        if let token = UserDefaults.standard.string(forKey: "token") {
            evaluateJavaScript("initApp(IOS, \"\(token)\")");
        } else {
            evaluateJavaScript("initApp(IOS, null");
        }
        doAfterLoadsAndClear(); // 스크립트 대기열 있으면 실행
        viewLayout.isHidden = true; // 상단바 + 백지 끄기
        viewProgress.isHidden = true; // 프로그레스 끄기
        
        if let urls = webView.url?.absoluteString.split(separator: "?")[0].split(separator: "/") {
            if (urls.count >= 4) {
                if (urls[1].elementsEqual(domain)) {
                    if (urls[2].elementsEqual("member")) {
                        if (urls[3].elementsEqual("login")) {
                            // 로그인 페이지일 경우
                            // 자동 로그인 정보가 있으면 실행
                            if UserDefaults.standard.bool(forKey: "autologin") {
                                script("$(\"#id\"      ).val(\"\(UserDefaults.standard.string(forKey: "id") ?? "")\")");
                                script("$(\"#password\").val(\"\(UserDefaults.standard.string(forKey: "pw") ?? "")\")");
                                script("$(\"#check_agree\").prop(\"checked\", true)");
                                script("login()");
                            }
                        }
                    } else if (urls[2].elementsEqual("weaningfood")) {
                        if (urls[3].elementsEqual("main")) {
                            if let redirect: String = UserDefaults.standard.string(forKey: "redirectByAlarm") {
                                // 알람으로 수신한 url이 있는 상태에서 main 화면에 진입 -> redirect
                                UserDefaults.standard.removeObject(forKey: "redirectByAlarm");
                                wvMain.load(URLRequest(url: URL(string: redirect)!));
                            }
                        }
                    }
                }
            }
            
            if (urls.count >= 4 && urls[3].elementsEqual("mobileConfirm")) {
                script("mobileAuth()")
            }
        }
    }
    
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        Print("didReceiveServerRedirectForProvisionalNavigation: \(String(describing: navigation))");
        viewLayout.isHidden = true;
        viewProgress.isHidden = true;
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        handleError(error, navigation: navigation);
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        handleError(error, navigation: navigation);
    }
    
    func handleError(_ error: Error, navigation: WKNavigation) {
        if let error = error as NSError? {
            Print("didFail: \(error.code)");
            
            var message = "오류 발생";
            var url = self.navigating;
            
            switch error.code {
            case 102: // CANCEL
                message = "잘못된 페이지입니다.(\(String(statusCode)))";
                url = nil;
                break;
                
            case -1001: // TIMED OUT
                message = "서버 연결이 원활하지 않습니다.";
                break;
                
            case -1003: // SERVER CANNOT BE FOUND
                message = "서버 연결이 원활하지 않습니다.";
                break;
                
            case -1100: // URL NOT FOUND ON SERVER
                message = "잘못된 페이지입니다.";
                url = nil;
                break;
                
            case -999: // url이 서로다를 경우
                return;
                
            default:
                break;
            }
            
            alert(wvMain, message: message) { () in
                // 계속 재접속 시도 중...
                // 그냥 현재 앱을 사용할 수 없다고 하고 종료시키는 게 맞을지?
                if let url = url {
                    self.wvMain.load(URLRequest(url: url));
                }
            };
        }
        viewLayout.isHidden = true;
        viewProgress.isHidden = true;
    }
    
    
    
}

/**
 * 브라우저 기본 기능 관리
 */
extension ViewController: WKUIDelegate {
    
    // alert 창
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
        Print("runJavaScriptAlertPanelWithMessage")
        alert(webView, message: message, complete: completionHandler);
    }
    
    func alert(_ webView: WKWebView, message: String, complete: @escaping () -> Void) {
        Print("alert")
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert);
        alert.addAction(UIAlertAction(title: "확인", style: .default, handler: { (action) in complete() }));
        self.present(alert, animated: true, completion: nil);
    }
    
    func alert(_ message: String) {
        alert(wvMain, message: message) { };
    }
    
    func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (Bool) -> Void) {
        confirm(message, completionHandler: completionHandler)
    }
    
    // confirm 창
    func confirm(_ message: String, completionHandler: @escaping (Bool) -> Void) {
        Print("confirm")
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert);
        alert.addAction(UIAlertAction(title: "확인", style: .default, handler: { (action) in completionHandler(true) }));
        alert.addAction(UIAlertAction(title: "취소", style: .default, handler: { (action) in completionHandler(false) }));
        self.present(alert, animated: true, completion: nil);
    }
    
    // window.open(), window.close() 처리
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        let frame = UIScreen.main.bounds
        
        createWebView = WKWebView(frame: frame, configuration: configuration)
        
        createWebView!.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        createWebView!.navigationDelegate = self
        createWebView!.uiDelegate = self
        
        wvMain.addSubview(createWebView!)
        
        return createWebView!
    }
    
    func webViewDidClose(_ webView: WKWebView) {
        if webView == createWebView {
            createWebView?.removeFromSuperview()
            createWebView = nil
        }
    }
 
}

/**
 * Javascript 연결
 * webkit.messageHandlers.함수명.postMessage(~);
 * common.js에서 callApp(함수명, ~);으로 호출 가능하게 만듦
 */
extension ViewController: WKScriptMessageHandler {
    
    // 안드로이드에선 @JavascriptInterface 어노테이션을 걸어주면 되는데
    // 아이폰에선 따로 함수명을 등록해줘야 함
    func initScriptMessageHandler() {
        let names = ["test", "onload", "history_back"
            , "setAutoLogin"
            , "loginNaver", "logoutNaver"
            , "loginKakao", "logoutKakao"
            , "loginFBook", "logoutFBook"
            , "openProgress", "closeProgress"
            , "getPushStatus", "setPushStatus"
            , "getAppAlarms", "insertAlarm", "updateAlarm", "deleteAlarm"
            , "getScaleDevice", "removeScaleDevice"
            , "startScanDevice", "stopScanDevice", "moveToAddDevice", "mailCall"
        ];
        for name in names {
            wvMain.configuration.userContentController.add(self, name: name);
        }
    }
    
    // webkit.messageHandlers[message.name].postMessage(message.body) 호출 시 동작
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        
        let name = message.name;
        Print("message.name: \(message.name)");
        let body = message.body as? String ?? ""
        Print("message.body: \(body)");
        
        jsInterface(name, body: body);
    }
    
    func jsInterface(_ name: String) {
        jsInterface(name, body: "");
    }
    func jsInterface(_ name: String, body: String) {
        switch name {
        case "test":
            script(String.init(format: "alert(\"\(body.replacingOccurrences(of: "\"", with: "\\\""))\")"));
            break;
            
        case "onload":
            //                isLoaded = true;
            //                // 스크립트 대기열 있으면 실행
            //                for script in afterLoads {
            //                    evaluateJavaScript(script);
            //                }
            //                afterLoads.removeAll();
            // doAfterLoadsAndClear() 만들어서 쓸 일 없을 듯
            break;
            
        case "history_back":
            wvMain.goBack();
            break;
            
        case "setAutoLogin":
            if let json = body.data(using: .utf8) {
                do {
                    let data = try JSONSerialization.jsonObject(with: json, options: []) as! [String:AnyObject];
                    let autologin = data["autologin"] as! Bool
                    if autologin {
                        let id = data["id"] as! String;
                        let pw = data["password"] as! String;
                        UserDefaults.standard.set(id, forKey: "id");
                        UserDefaults.standard.set(pw, forKey: "pw");
                        UserDefaults.standard.set(autologin, forKey: "autologin");
                        
                    } else {
                        UserDefaults.standard.removeObject(forKey: "autologin");
                    }
                } catch {
                    Print(error.localizedDescription);
                }
            }
            break;
            
        case "loginNaver":
            naver.requestThirdPartyLogin();
            break;
            
        case "logoutNaver":
            naver.resetToken();
            break;
            
        case "loginKakao":
            if let session = KOSession.shared() {
                session.close();
                session.open { (error) in
                    ViewController.this?.onKaKaoSessionOpened(session);
                }
            }
            break;
            
        case "logoutKakao":
            if let session = KOSession.shared() {
                session.close();
            }
            break;
            
        case "loginFBook":
            FBSDKLoginManager.init().logIn(withReadPermissions: ["public_profile"], from: self) { (result, error) in
                ViewController.this?.onFBLogined(result, error);
            }
            break;
            
        case "logoutFBook":
            FBSDKLoginManager.init().logOut();
            break;
            
        case "openProgress":
            viewProgress.isHidden = false;
            break;
            
        case "closeProgress":
            viewProgress.isHidden = true;
            break;
            
        case "getPushStatus":
            getPushStatus();
            break;
            
        case "setPushStatus":
            confirm("허용 여부 설정을 위해 앱 설정 화면으로 이동하시겠습니까?") { (ok) in
                if (ok) {
                    let url = URL(string: UIApplication.openSettingsURLString);
                    UIApplication.shared.open(url!, options: [:], completionHandler: { (complete) in
                        ViewController.this?.Print("complete");
                    });
                } else {
                    ViewController.this?.getPushStatus();
                }
            }
            break;
            
        case "getAppAlarms":
            if let json = UserDefaults.standard.string(forKey: "alarms") {
                script("onGetAppAlarms(\"\(json.replacingOccurrences(of: "\"", with: "\\\""))\")");
            } else {
                script("onGetAppAlarms(\"[]\")");
            }
            break;
            
        case "insertAlarm":
            do {
                if let data = body.data(using: String.Encoding.utf8) {
                    var alarm = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any];
                    alarm["index"] = String(Date().timeIntervalSince1970);
                    
                    var list = getAlarmList();
                    list.append(alarm);
                    setAppAlarms(list);
                }
            } catch {
                Print("1234");
            }
            break;
            
        case "updateAlarm":
            do {
                if let data = body.data(using: String.Encoding.utf8) {
                    let alarm = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any];
                    if let index = alarm["index"] as? String {
                        var list = getAlarmList();
                        let i = findAlarmIndex(list, index: index);
                        if (i >= 0) {
                            list[i] = alarm;
                            setAppAlarms(list);
                        }
                    }
                }
            } catch {
                Print("asdf");
            }
            break;
            
        case "deleteAlarm":
            let index = body;
            var list = getAlarmList();
            let i = findAlarmIndex(list, index: index);
            if (i >= 0) {
                list.remove(at: i);
                setAppAlarms(list);
            }
            break;
            
        case "getScaleDevice":
            if let device = UserDefaults.standard.string(forKey: "device") {
                script("onGetScaleDevice(\"\(device)\")");
            } else {
                script("onGetScaleDevice(null)");
            }
            break;
            
        case "removeScaleDevice":
            UserDefaults.standard.removeObject(forKey: "device");
            break;
            
        case "startScanDevice":
            startScanDevice();
            break;
            
        case "stopScanDevice":
            stopScanDevice();
            break;
            
        case "moveToAddDevice":
            moveToAddDevice();
            break;
            
        case "mailCall" :
            //script(String.init(format: "alert(\"\(body.replacingOccurrences(of: "\"", with: "\\\""))\")"));
            mailCall();
            break;
        default:
            break;
        }
    }
    
    func getPushStatus() {
        let on = UIApplication.shared.isRegisteredForRemoteNotifications;
        script("onGetPushStatus(\(String(on)))");
    }
    
    func getAlarmList() -> [[String:Any]] {
        if let appDelegate: AppDelegate = UIApplication.shared.delegate as? AppDelegate {
            return appDelegate.getAlarmList();
        }
        return [];
    }
    
    func findAlarmIndex(_ list: [[String:Any]], index: String) -> Int {
        for i in 0..<list.count {
            let item = list[i];
            if let itemIndex = item["index"] as? String {
                if index == itemIndex {
                    return i;
                }
            }
        }
        return -1;
    }
    
    func setAppAlarms(_ list: [[String:Any]]) {
        do {
            let json = String(data: try JSONSerialization.data(withJSONObject: list, options: []), encoding: .utf8);
            UserDefaults.standard.set(json, forKey: "alarms");
            
            let nc = UNUserNotificationCenter.current();
            nc.removeAllPendingNotificationRequests();
            
            for alarm in list {
                let id = alarm["index"] as! String;
                let type = alarm["type"] as! Int;
                
                let content = UNMutableNotificationContent();
                content.title = "\(AppDelegate.alarmTypes[type]) 알림입니다.";
                //content.body = "타입: \(String(alarm["type"] as! Int))";
                
                let times = (alarm["time"] as! String).split(separator: ":");
                var dc = DateComponents();
                dc.hour   = Int(times[0]);
                dc.minute = Int(times[1]);
                let trigger = UNCalendarNotificationTrigger(dateMatching: dc, repeats: true);
                
                nc.add(UNNotificationRequest(identifier: id, content: content, trigger: trigger));
            }
            
        } catch {
            
        }
    }
}

class AlarmNotificationContent: UNMutableNotificationContent {
    var redirectUrl: String?;
}

/**
 * SNS callback
 */
extension ViewController {
    func onKaKaoSessionOpened(_ session: KOSession) {
        if session.isOpen() {
            Print("login successed");
            let script = String.init(
                format: "afterAppKakaoLogin(\"%@\", \"%@\")"
                , session.token.accessToken
                , session.token.refreshToken);
            Print(script);
            self.script(script);
        } else {
            Print("login failed");
        }
    }
    
    func onFBLogined(_ result: FBSDKLoginManagerLoginResult?, _ error: Error?) {
        if let error = error {
            onFBError(error);
        } else if let cancelled = result?.isCancelled {
            if (cancelled) {
                onFBCancel();
                return;
            }
        }
        onFBSuccess(result);
    }
    func onFBSuccess(_ result: FBSDKLoginManagerLoginResult?) {
        Print("FBCallback onSuccess");
        if let token = FBSDKAccessToken.current()?.tokenString {
            let script = String.init(format: "afterAppFBookLogin(\"%@\")", token);
            Print(script);
            self.script(script);
        } else {
            onFBCancel();
        }
    }
    func onFBCancel() {
        Print("FBCallback onCancel");
    }
    func onFBError(_ error: Error) {
        Print("FBCallback onError: \(error)");
    }
    
    func handleResult(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool? {
        
        // 네이버 콜백일 경우
        if url.absoluteString.starts(with: "naverlogin://") {
            handleNaver(url);
            return nil;
        }
        
        // 카카오 콜백일 경우
        if KOSession.isKakaoAccountLoginCallback(url) {
            return handleKakao(url);
        }
        
        // 페이스북 콜백일 경우
        if let handled = isHandleFBook(app, open: url, options: options) {
            if let token = FBSDKAccessToken.current()?.tokenString {
                self.script("afterAppFBookLogin(\"\(token)\")");
                return handled;
            }
        }
        
        return nil;
    }
    
    func handleNaver(_ url: URL) {
        if UIApplication.shared.canOpenURL(URL(fileURLWithPath: "naversearchapp://")) {
            Print("canOpenURL");
            if let receiveType = NaverThirdPartyLoginConnection.getSharedInstance()?.receiveAccessToken(url) {
                Print("receiveType?");
                Print(receiveType);
                switch (receiveType) {
                case SUCCESS:
                    break;
                case PARAMETERNOTSET:
                    break;
                case CANCELBYUSER:
                    break;
                case NAVERAPPNOTINSTALLED:
                    break;
                case NAVERAPPVERSIONINVALID:
                    break;
                case OAUTHMETHODNOTSET:
                    break;
                default:
                    break;
                }
            }
        }
    }
    
    func handleKakao(_ url: URL) -> Bool {
        return KOSession.handleOpen(url);
    }
    
    func isHandleFBook(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool? {
        return FBSDKApplicationDelegate.sharedInstance()?.application(app
            , open: url
            , sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String
            , annotation: options[UIApplication.OpenURLOptionsKey.annotation]);
    }
}

extension ViewController: NaverThirdPartyLoginConnectionDelegate {
    
    func oauth20ConnectionDidOpenInAppBrowser(forOAuth request: URLRequest!) {
        Print("oauth20ConnectionDidOpenInAppBrowser");
        loginAfterAuth();
    }
    
    func oauth20ConnectionDidFinishRequestACTokenWithAuthCode() {
        Print("oauth20ConnectionDidFinishRequestACTokenWithAuthCode");
        loginAfterAuth();
    }
    
    func oauth20ConnectionDidFinishRequestACTokenWithRefreshToken() {
        Print("oauth20ConnectionDidFinishRequestACTokenWithRefreshToken");
        loginAfterAuth();
    }
    
    func oauth20ConnectionDidFinishDeleteToken() {
        Print("oauth20ConnectionDidFinishDeleteToken");
    }
    
    func oauth20Connection(_ oauthConnection: NaverThirdPartyLoginConnection!, didFailWithError error: Error!) {
        Print("oauth20Connection didFailWithError");
    }
    
    func oauth20Connection(_ oauthConnection: NaverThirdPartyLoginConnection!, didFailAuthorizationWithRecieveType recieveType: THIRDPARTYLOGIN_RECEIVE_TYPE) {
        Print("oauth20Connection didFailAuthorizationWithRecieveType");
    }
    
    func oauth20Connection(_ oauthConnection: NaverThirdPartyLoginConnection!, didFinishAuthorizationWithResult recieveType: THIRDPARTYLOGIN_RECEIVE_TYPE) {
        Print("oauth20Connection didFinishAuthorizationWithResult");
    }
    
    func loginAfterAuth() {
        let script = String.init(
            format: "afterAppNaverLogin(\"%@\", \"%@\")"
            , naver.accessToken
            , naver.refreshToken);
        Print(script);
        self.script(script);
    }
}

/**
 * 체중계 관련
 */
extension ViewController {
    func loadProfile() -> QingNiuUser {
        var id = UserDefaults.standard.string(forKey: "id");
        var birthdayString = UserDefaults.standard.string(forKey: "birthday");
        var height = UserDefaults.standard.integer(forKey: "height");
        let sex = UserDefaults.standard.integer(forKey: "sex");
        
        if id == nil { id = "CHIRON" }
        if birthdayString == nil { birthdayString = "1900-01-01" }
        if height == 0 { height = 170 }
        
        return QingNiuUser(userWithUserId: id, andHeight: CGFloat(height), andGender: Int32(sex), andBirthday: birthdayString)
    }
    
    func startScanDevice() {
        Print("startScanDevice");
        if let device = UserDefaults.standard.string(forKey: "device") {
            if let sn = sn {
                sn.viewController = self;
                sn.setUser(user: loadProfile());
                sn.setUnit(unit: QingNiuWeightUnit.registerWeightUnitKg);
                sn.setDevice(device: device);
                sn.setServiceOn(on: true);
            }
        } else {
            alert("설정된 디바이스가 없습니다.");
        }
    }
    func bleServiceChangedTo(_ on: Bool) {
        Print("블루투스 서비스 \(on ? "ON" : "OFF")");
        script("setBleServiceStatus(\(String(on)))");
    }
    func stopScanDevice() {
        if let sn = sn {
            if sn.serviceIsOn() {
                sn.setServiceOn(on: false);
            }
        }
    }
    func moveToAddDevice() {
        if let storyboard = storyboard {
            requestCode = REQUEST_ADD_SERVICE;
            let view = storyboard.instantiateViewController(withIdentifier: "addDevice");
            present(view, animated: true, completion: nil);
        }
    }
    /**
    * 메일앱과 연동
    */
    func mailCall() {
        let email = "korea@gaonn.net"
        if let url = URL(string: "mailto:\(email)") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func onConnected(_ device: QingNiuDevice) {
        
    }
    
    func onDisconnected(_ device: QingNiuDevice) {
        
    }
    
    func onUnsteadyWeight(_ v: Float) {
        script("onUnsteadyWeight(\(String(v)))");
    }
    
    func onReceivedData(_ qnData: QNData) {
        let weight = qnData.weight;
        var fat = Float(0);
        
        if let datas = qnData.getAll() {
            for data in datas {
                if let data = data as? QNItemData {
                    if data.type == BodyFat {
                        fat = data.value;
                    }
                }
            }
        }
        script("onReceivedData(\(String(weight)), \(String(fat)))");
    }
    func onReceivedStoreData(_ list: [QNData]) {
        for data in list {
            onReceivedData(data);
        }
    }
}
