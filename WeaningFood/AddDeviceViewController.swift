//
//  AddDeviceViewController.swift
//  WeaningFood
//
//  Created by ghsong on 20/12/2018.
//  Copyright © 2018 chironsoft. All rights reserved.
//

import UIKit

class AddDeviceViewController: UIViewController {
    
    var sn: SharedBluetooth?;

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnScan: UIButton!
    
    @IBOutlet weak var tbDevices: UITableView!
    
    var devices: [QingNiuDevice] = [];
    
    override func viewDidLoad() {
        super.viewDidLoad();
        sn = SharedBluetooth.getInstance();
        scan(true);
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent;
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        dismiss(animated: true, completion: nil);
    }
    
    @IBAction func onClickScan(_ sender: Any) {
        if let sn = sn {
            scan(!sn.isScanning());
        }
    }
    
    func scan(_ scan: Bool) {
        if scan {
            devices.removeAll();
            tbDevices.reloadData();
            startScan();
            btnScan.setTitle("스캔 중지", for: .normal);
        } else {
            sn?.stopScan();
            btnScan.setTitle("스캔 시작", for: .normal);
        }
    }
    
    func startScan() {
        class ScanDelegate: NSObject, QNBleScanDelegate {
            var controller: AddDeviceViewController;
            init(_ controller: AddDeviceViewController) {
                self.controller = controller;
            }
            func onScan(_ device: QingNiuDevice!) {
                print("onScan");
                if let device = device {
                    print("name:" + device.name + " mac:" + device.macAddress + " model:" + device.model);
                    for item in controller.devices {
                        if (item.macAddress.elementsEqual(device.macAddress)) {
                            return;
                        }
                    }
                    controller.devices.append(device);
                    controller.tbDevices.reloadData();
                }
            }
        }
        if let sn = sn {
            sn.scanNewDevice(delegate: ScanDelegate(self));
        }
    }
}

extension AddDeviceViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return devices.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let device = devices[indexPath.row];
        let cell = tableView.dequeueReusableCell(withIdentifier: "deviceCell") as! DeviceItem;
        return cell.set(device);
    }
}

extension AddDeviceViewController: UITableViewDelegate {
    func loadProfile() -> QingNiuUser {
        var id = UserDefaults.standard.string(forKey: "id");
        var birthdayString = UserDefaults.standard.string(forKey: "birthday");
        var height = UserDefaults.standard.integer(forKey: "height");
        let sex = UserDefaults.standard.integer(forKey: "sex");
        
        if id == nil { id = "CHIRON" }
        if birthdayString == nil { birthdayString = "1900-01-01" }
        if height == 0 { height = 170 }
        
        return QingNiuUser(userWithUserId: id, andHeight: CGFloat(height), andGender: Int32(sex), andBirthday: birthdayString)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let device = devices[indexPath.row];
        sn?.setUser(user: loadProfile());
        UserDefaults.standard.set(device.macAddress, forKey: "device");
        dismiss(animated: true, completion: nil);
    }
}
