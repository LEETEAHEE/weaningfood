//
//  SharedBluetooth.swift
//  WeaningFood
//
//  Created by ghsong on 2018. 11. 23..
//  Copyright © 2018년 chironsoft. All rights reserved.
//

import UIKit

class SharedBluetooth: NSObject {
    public static let TAG = "SharedBluetooth";
    
    var qm: QNBleManager?;
    var viewController: ViewController?;
    
    private static let instance = SharedBluetooth();
    private override init() {
        super.init();
        self.qm = QNBleManager.getInstance(self);
    }
    static func getInstance() -> SharedBluetooth {
        return instance;
    }
    
    func setServiceOn(on: Bool) {
        if let qm = qm {
            qm.setServiceOn(on);
            if let vc = viewController {
                vc.bleServiceChangedTo(on);
            }
        }
    }
    func serviceIsOn() -> Bool {
        if let qm = qm { return qm.serviceIsOn(); }
        return false;
    }
    func setUser(user: QingNiuUser) {
        qm?.user = user;
    }
    func setUnit(unit: QingNiuWeightUnit) {
        if let qm = qm { qm.setUnit(unit); }
    }
    func isScanning() -> Bool {
        if let qm = qm { return qm.isScanningNewDevice(); }
        return false;
    }
    
    func scanNewDevice(delegate: QNBleScanDelegate) {
        if let qm = qm { qm.scanNewDevice(delegate); }
    }
    func stopScan() {
        if let qm = qm { qm.stopScan(); }
    }
    
    func setDevice(device: String) {
        if let qm = qm {
            qm.removeAllDevices();
            qm.addDevice(device);
        }
    }
}
extension SharedBluetooth: QNBleDelegate {
    func onConnectStart(_ device: QingNiuDevice!) {
        print("onConnectStart");
    }
    
    func onConnected(_ device: QingNiuDevice!) {
        print("onConnected");
        if let vc = viewController {
            vc.onConnected(device);
        }
    }
    
    func onDisconnected(_ device: QingNiuDevice!) {
        print("onDisconnected");
        if let vc = viewController {
            vc.onDisconnected(device);
        }
//        if first
    }
    
    func onUnsteady(_ device: QingNiuDevice!, weight: Float) {
        print("onUnsteady: %f", weight);
        if let vc = viewController {
            vc.onUnsteadyWeight(weight);
        }
    }
    
    func onReceived(_ device: QingNiuDevice!, data: NSMutableDictionary!) {
        print("onReceived");
        
        if let vc = viewController {
//            vc.toast();
            if let qnData = QNData(data: data as? [AnyHashable : Any]) {
                vc.onReceivedData(qnData);
            }
        }
        if let qm = qm {
            if qm.isScanningNewDevice() {
                afterFirstReceive(device);
            }
        }
    }
    private func afterFirstReceive(_ qnBleDevice: QingNiuDevice) {
        let deviceMac = qnBleDevice.macAddress;
//        if (first)
        
        UserDefaults.standard.set(deviceMac, forKey: "device");
    }
    
    func onReceived(_ device: QingNiuDevice!, storeData datas: [NSMutableDictionary]!) {
        print("onReceived");
        
        if let vc = viewController {
            //            vc.toast();
            var list:[QNData] = [];
            for data in datas {
                if let qnData = QNData(data: data as? [AnyHashable : Any]) {
                    list.append(qnData);
                }
            }
            vc.onReceivedStoreData(list);
        }
    }
    
    func onReceivedModelUpdate(_ device: QingNiuDevice!) {
        print("onReceivedModelUpdate");
    }
    
}
